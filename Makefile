SHELL = /bin/bash

build_tag ?= sidekiq-web

chart_path = ./chart/sidekiq-web
chart_name = $(shell cat $(chart_path)/Chart.yaml | grep ^name | awk '{print $$2}')
chart_version = $(shell cat $(chart_path)/Chart.yaml | grep ^version | awk '{print $$2}')
chart_package = $(chart_name)-$(chart_version).tgz
chart_release ?= sidekiq-web


.PHONY : build
build:
	docker build -t $(build_tag) .

.PHONY : test
test:
	if [[ "$(CI)" != "" ]]; then docker pull $(build_tag); fi
	build_tag=$(build_tag) ./test.sh

.PHONY : lock
lock:
	docker run --rm -v "$(shell pwd):/app" -w /app ruby:3.2 \
		/bin/bash -c 'gem install bundler; bundle lock'

.PHONY: lint-chart
lint-chart:
	helm lint $(chart_path)

$(chart_package):
	helm package $(chart_path)

.PHONY: package-chart
package-chart: $(chart_package)

ifdef CI
.PHONY: helm-login
helm-login:
	helm registry login -u gitlab-ci-token -p $(CI_JOB_TOKEN) $(CI_REGISTRY)

.PHONY: chart
chart: helm-login $(chart_package)
	helm push $(chart_package) oci://$(CI_REGISTRY_IMAGE)/chart
endif
