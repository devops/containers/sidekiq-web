require 'sidekiq'

Sidekiq.configure_client do |config|
  config.redis = { url: ENV.fetch('REDIS_URL', 'redis://localhost:6379'), size: 1 }
end

require 'securerandom'
secret_key = SecureRandom.hex(32)

require 'delegate' # https://github.com/rack/rack/pull/1610
require 'rack/session/cookie'
use Rack::Session::Cookie, secret: secret_key, same_site: true, max_age: 86400

require 'sidekiq/web'
run Sidekiq::Web
