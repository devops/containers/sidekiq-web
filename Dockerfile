FROM ruby:3.2

WORKDIR /app

COPY . .

RUN set -eux; \
    gem install bundler -v "$(tail -1 Gemfile.lock | tr -d ' ')"; \
    bundle install; \
    chmod -R g=u .

EXPOSE 9292 9293

CMD [ "bundle", "exec", "rackup", "-o", "0.0.0.0" ]
