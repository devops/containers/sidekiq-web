#!/bin/bash

docker-compose up -d

code=1
interval=5
timeout=60

SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    echo -n "---> Checking status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' sidekiq-web-candidate)
    echo $status

    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

if [[ $code -eq 0 ]]; then
    echo "SUCCESS" >&2
else
    echo "FAILURE" >&2
fi

docker-compose down

exit ${code}
